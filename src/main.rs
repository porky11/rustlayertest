extern crate sdl2;
extern crate specs;
extern crate specs_derive;

use sdl2::{
    pixels::Color,
    render::{Canvas, Texture},
    video::Window,
    event::Event,
    keyboard::Keycode,
    rect::Rect,
    ttf::Font
};

use std::time::Duration;

mod layer;

use layer::{Layer, LayerManager};

struct Context<'ttf, 'r> {
    canvas: Canvas<Window>,
    default_font: Font<'ttf, 'r>
}

struct Title;

impl<'ttf, 'r> Layer<'r, Context<'ttf, 'r>, Event> for Title {
    fn input(&mut self, context: &mut Context<'ttf, 'r>, event: &Event) -> layer::Change<'r, Context<'ttf, 'r>, Event> {
        use Event::*;
        use layer::Change::*;
        match event {
            Quit {..} => Close,
            KeyDown {keycode: Some(keycode), ..} => match keycode {
                Keycode::Escape | Keycode::Return => Add(Box::new(Options::new(context))),
                _=> Stay
            }
            _ => Defer
        }
    }

    fn update(&mut self, _c: &mut Context<'ttf, 'r>) -> layer::Change<'r, Context<'ttf, 'r>, Event> {
        use layer::Change::*;
        //println!("It's me, Title!");
        Stay
    }
}

struct DrawBox<'r> {
    texture: Texture<'r>,
    rect: Rect,
}

impl<'ttf, 'r> DrawBox<'r> {
    fn from_text(context: &mut Context<'ttf, 'r>, text: &str, rect: Rect) -> Self {
        let surface = context.default_font.render(text)
            .blended(Color::RGBA(255, 0, 0, 255)).unwrap();
        let texture =
            context.canvas.texture_creator().create_texture_from_surface(&surface).unwrap();
        Self {texture, rect}
    }
}

static size: u32 = 80;
static res: (u32, u32) = (16, 9);

struct Options<'r> (
    Vec<DrawBox<'r>>
);

impl<'ttf, 'r> Options<'r> {
    fn new(context: &mut Context<'ttf, 'r>) -> Self {
        let mut options = Vec::new();
        for (i, text) in vec![
            "Adventures on the sea",
            "Abduction by the nature army",
            "Lonely in the village",
        ].iter().enumerate() {
            let i = i as u32;
            options.push(
                DrawBox::from_text(
                    context,
                    text,
                    Rect::new(
                        (size * (1 + i * 5)) as i32,
                        (size * 2) as i32,
                        size * 4,
                        size
                    )
                )
            );
        }
        Options(options)
    }
}

impl<'ttf, 'r> Layer<'r, Context<'ttf, 'r>, Event> for Options<'r> {
    fn input(&mut self, _c: &mut Context<'ttf, 'r>, event: &Event) -> layer::Change<'r, Context<'ttf, 'r>, Event> {
        use Event::*;
        use layer::Change::*;
        match event {
            Quit {..} => Remove,
            KeyDown {keycode: Some(keycode), ..} => match keycode {
                Keycode::Escape | Keycode::Return => Remove,
                _=> Stay
            }
            _ => Defer
        }
    }

    fn passive_update(&mut self, c: &mut Context<'ttf, 'r>) {
        c.canvas.set_draw_color(Color::RGB(0, 0, 0));
        for text_box in self.0.iter() {
            c.canvas.draw_rect(text_box.rect).unwrap();
        }
    }

    fn update(&mut self, _c: &mut Context<'ttf, 'r>) -> layer::Change<'r, Context<'ttf, 'r>, Event> {
        use layer::Change::*;
        //println!("It's me, Options!");
        Stay
    }
}


pub fn main() {
    let sdl = sdl2::init().unwrap();
    let video = sdl.video().unwrap();
    let ttf = sdl2::ttf::init().unwrap();

    let window = video.window("Story Game", size * res.0, size * res.1)
        .position_centered()
        .build()
        .unwrap();

    let mut canvas = window.into_canvas().build().unwrap();

    canvas.set_draw_color(Color::RGB(255, 255, 255));
    canvas.clear();
    canvas.present();
    

    let mut default_font = ttf.load_font("DejaVuSerif.ttf", 128).unwrap();
    default_font.set_style(sdl2::ttf::STYLE_BOLD);
    
    let mut context = Context {canvas, default_font};

    let mut event_pump = sdl.event_pump().unwrap();

    let mut manager = LayerManager::new();
    manager.add(Title);

    while manager.is_active() {
        context.canvas.set_draw_color(Color::RGB(255, 255, 255));
        context.canvas.clear();

        for event in event_pump.poll_iter() {
            manager.input(&mut context, event);
        };

        manager.update(&mut context);

        context.canvas.present();
        ::std::thread::sleep(Duration::new(0, 1_000_000_000u32 / 60));
    }
}

