pub enum Change<'r, C, E> {
    Stay,
    Defer,
    Add(Box<Layer<'r, C, E> + 'r>),
    Remove,
    Switch(Box<Layer<'r, C, E> + 'r>),
    Close
}

pub trait Layer<'r, C, E> {

    /// Executed for top layer per input and optionally for more layers.
    fn input(&mut self, _c: &mut C, _e: &E) -> Change<'r, C, E> {Change::Stay}

    /// Executed when the layer is activated.
    fn activate(&mut self) {}

    /// Executed when the layer is deactivated.
    fn deactivate(&mut self) {}

    /// Executed for all layers from bottom to top.
    fn passive_update(&mut self, _c: &mut C) {}

    /// Executed for top layer and optionally for more layers.
    fn update(&mut self, _c: &mut C) -> Change<'r, C, E>;

}

pub struct LayerManager<'r, C, E>(
    Vec<Box<Layer<'r, C, E> + 'r>>
);

impl<'r, C, E> LayerManager<'r, C, E> {
    pub fn new() -> Self {
        let list = Vec::new();
        LayerManager::<C, E>(list)
    }

    pub fn add<L: Layer<'r, C, E> + 'r>(&mut self, mut layer: L) {
        layer.activate();
        self.0.push(Box::new(layer) as Box<Layer<'r, C, E> + 'r>);
    }
    
    pub fn is_active(&self) -> bool {
        !self.0.is_empty()
    }

    fn change_layer (&mut self, change: Change<'r, C, E>, index: usize) {
        use self::Change::*;
        match change {
            Add(mut new_layer) => {
                new_layer.activate();
                self.0.insert(index + 1, new_layer);
            },
            Remove => {
                let mut old_layer = self.0.remove(index);
                old_layer.deactivate();
            },
            Switch(mut new_layer) => {
                let mut old_layer = self.0.remove(index);
                old_layer.deactivate();
                new_layer.activate();
                self.0.insert(index, new_layer);
            },
            Close => self.0.clear(),
            _ => ()
        }
    }

    pub fn input(&mut self, c: &mut C, e: E) {
        let (change, index) = 'change: loop {
            use self::Change::*;
            for (i, layer) in self.0.iter_mut().enumerate().rev() {
                match layer.input(c, &e) {
                    Defer => continue,
                    change => break 'change (change, i)
                }
            };
            break (Defer, 0);
        };
        self.change_layer(change, index);
    }

    pub fn update(&mut self, c: &mut C) {
        let (change, index) = 'change: loop {
            use self::Change::*;
            for (i, layer) in self.0.iter_mut().enumerate().rev() {
                match layer.update(c) {
                    Defer => continue,
                    change => break 'change (change, i)
                }
            };
            break (Defer, 0);
        };
        self.change_layer(change, index);

        for layer in self.0.iter_mut() {
            layer.passive_update(c);
        }
    }
}


